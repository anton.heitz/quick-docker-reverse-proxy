#!/bin/bash
#@author:Anton Heitz

# setup variables
echo 
echo Setting up the reverse proxy. Press Enter to use the default Value
echo 
read -p 'Please enter the server URL [localhost]: ' SERVER_URL
SERVER_URL=${SERVER_URL:-localhost}
read -p 'Please enter the server Port [80]: ' SERVER_PORT
SERVER_PORT=${SERVER_PORT:-80}
read -p 'Please enter the desired Timeout in Seconds [9999]: ' TIMEOUT
TIMEOUT=${TIMEOUT:-9999}
read -p 'Please enter the desired maximum request size in mb [200]: ' MAX_BODYSIZE
MAX_BODYSIZE=${MAX_BODYSIZE:-200}
echo
echo Setting up the proxy destinations:
echo
read -p 'Please enter the destination hostname for the root URL ("/") [localhost]: ' ROOT_PROXY
ROOT_PROXY=${ROOT_PROXY:-localhost}
read -p 'Please enter the destination port for the root URL ("/") [8080]: ' ROOT_PORT
ROOT_PORT=${ROOT_PORT:-8080}
read -p 'Does this ressource requre https? [y/N] ' ROOT_HTTPS_F
ROOT_HTTPS_F=${ROOT_HTTPS_F:-N}
echo

ROOT_HTTPS=""
if [ $ROOT_HTTPS_F == "Y" ] || [ $ROOT_HTTPS_F == "y" ] ; then
    ROOT_HTTPS="s"
fi

read -p 'Do you want to add another proxy? [y/N]: ' MORE_DESTINATIONS
MORE_DESTINATIONS=${MORE_DESTINATIONS:-N}
DESTINATIONS=()

while [ $MORE_DESTINATIONS == "y" -o $MORE_DESTINATIONS == "Y" ]; do
echo
read -p 'Please enter the new proxy subroute ("api" will result in "/api") [api]: ' NEW_API_ENDPOINT
NEW_API_ENDPOINT=${NEW_API_ENDPOINT:-api}
read -p 'Please enter the new proxy destination hostname [localhost]: ' NEW_API_DESTINATION
NEW_API_DESTINATION=${NEW_API_DESTINATION:-localhost}
read -p 'Please enter the new proxy destination port [8090]: ' NEW_API_PORT
NEW_API_PORT=${NEW_API_PORT:-8090}

read -p 'Does this ressource requre https? [y/N] ' NEW_API_HTTPS
NEW_API_HTTPS=${NEW_API_HTTPS:-N}

DESTINATIONS+=($NEW_API_ENDPOINT $NEW_API_DESTINATION $NEW_API_PORT $NEW_API_HTTPS)

echo
# redo loop
read -p 'Do you want to add another proxy? [y/N] ' MORE_DESTINATIONS2
MORE_DESTINATIONS=${MORE_DESTINATIONS2:-N}
done

# create other proxys string
OTHER_PROXYS=""
for ((i=0; i<${#DESTINATIONS[@]}; i+=4)); do
    OTHER_PROXYS+="\\tlocation \/${DESTINATIONS[i]} {\\n"
    OTHER_PROXYS+="\\t\\tproxy_pass http"
    if [ ${DESTINATIONS[i+3]} == "Y" ] || [ ${DESTINATIONS[i+3]} == "y" ] ; then
        OTHER_PROXYS+="s"
    fi
    OTHER_PROXYS+=":\/\/${DESTINATIONS[i+1]}:${DESTINATIONS[i+2]};\\n"
    OTHER_PROXYS+="\\t}\\n\\n"
done
echo
echo Creating nginx configurations...
echo
# create server block
sed -e "s/\${SERVER_URL}/$SERVER_URL/" -e "s/\${SERVER_PORT}/$SERVER_PORT/" \
    -e "s/\${TIMEOUT}/$TIMEOUT/"  \
    -e "s/\${MAX_BODYSIZE}/$MAX_BODYSIZE/" \
    -e "s/\${ROOT_PROXY}/$ROOT_PROXY/" \
    -e "s/\${ROOT_PORT}/$ROOT_PORT/" \
    -e "s/\${ROOT_HTTPS}/$ROOT_HTTPS/" \
    -e "s/\${OTHER_PROXYS}/${OTHER_PROXYS}/" \
    nginx_server_block > tmp_nginx_server_block 

read -p 'Please enter a unique name for the docker images and container [nginx-rev-proxy]: ' DOCKER_NAME
DOCKER_NAME=${DOCKER_NAME:-nginx-rev-proxy}

# build docker image
docker build -t $DOCKER_NAME .

# delete tmp file
rm tmp_nginx_server_block

# run docker image
docker run -d --name $DOCKER_NAME --network host $DOCKER_NAME
