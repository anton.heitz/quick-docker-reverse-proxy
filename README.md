# Quick Docker Reverse Proxy

This docker image will act as a local reverse proxy (like a localy installed nginx) and is ment to be easly deployable for development purposes.

## Install

Be sure docker is installed on you system.

To configure and create+run the proxy just execute the `./run_docker.sh` script and answer all the questions.

## NOTES

Be aware that if you define the ressource `/api` the request will be passed with this route to the defined destination server.

Example: if you want to call the `/getUsers` endpoint of the destination server defined under the `/api` ressource, the destination server will recieve the request at `/api/getUsers`.